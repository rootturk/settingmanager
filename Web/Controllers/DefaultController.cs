﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            ConfigurationReader read = new ConfigurationReader("", "", 5);

            var key = read.GetValue<string>("SiteKey");
            Console.WriteLine(key);
            Console.Read();

            return View();
        }
    }
}