﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Core
{
    public class ConfigurationReader
    {
        MemoryCache mCache = MemoryCache.Default;
        JavaScriptSerializer js = new JavaScriptSerializer();

        private readonly string _applicationName;
        private readonly string _connectionString;
        private readonly int _refreshTimeInterval;

        public ConfigurationReader(string applicationName, string connectionString, int refreshTimeInterval)
        {
            _applicationName = applicationName;
            _connectionString = connectionString;
            _refreshTimeInterval = refreshTimeInterval;
        }

        public T GetValue<T>(string key)
        {
            var response = new object();
            var data = new object();

            CacheItemPolicy policy = new CacheItemPolicy();
            CacheItem cItem = mCache.GetCacheItem("settings_last");

            if (cItem == null)
            {
                data = System.IO.File.ReadAllText(@"C:\Users\KEPLER\Documents\Visual Studio 2017\Projects\Settings.API\Data\Static\DB.json");
                mCache.Add("settings_last", data, DateTimeOffset.Now.AddMinutes(_refreshTimeInterval));
            }
            else
            {
                data = mCache.Get("settings_last") as string;
            }

            if (data != null)
            {
                var jsonObject = js.Deserialize<Model.Settings.Site>((string)data);
                foreach (var item in jsonObject.SiteSettings.Where(x => x.IsActive && x.ApplicationName == _applicationName))
                {
                    System.Type type = typeof(Model.Settings.SiteSettings);
                    System.Reflection.PropertyInfo[] properties = type.GetProperties();

                    var property = item.GetType().GetProperty("Name");
                    var value = property.GetValue(item);

                    if (value as string == key)
                    {
                        var nestedProperty = item.GetType().GetProperty("Value");
                        var nestedValue = nestedProperty.GetValue(item);

                        return (T)nestedValue;
                    }
                }
            }

            return (T)response;
        }

    }
}
