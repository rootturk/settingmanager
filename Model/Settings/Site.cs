﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Settings
{
    public class SiteSettings
    {
        public int Id { get; set;}
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public string ApplicationName { get; set; }


    }

    public class Site
    {
        public List<SiteSettings> SiteSettings { get; set; }
    }
}
